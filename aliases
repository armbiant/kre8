#!/bin/bash

####################################################################################################
#
# TIPS:
#
# turn on loadbalancing webnode from 1 pod/node to 3 pods/nodes:
# `kubectl patch deploy/webnode -p='{"spec":{"replicas":3}}'`
#
# turn on elastic scaling with min/max range, triggered by CPU percentages:
# `kubectl autoscale deployment webnode --min=2 --max=3 --cpu-percent=60   deployment "webnode-deployment" autoscaled`
#
# INSPECTING K8 PODS:
#   # like docker attach -- pop into running pod and look around.  find podnames using `kall` below.
#   `kubectl  exec  -it  webnode-66ff56dc7-5scq9  zsh`
#   root@webnode-66ff56dc7-5scq9 (xenial)  8:57PM /
#
#   You can find more info about that pod (eg: what VM/host it is running on and more) via:
#    `kubectl  describe  po/webnode-66ff56dc7-5scq9`
#
####################################################################################################
PV_DIR=/1/pv

function IP() {
  # returns IP addy of given hostname
  host "$1" 2> /dev/null |egrep '[0-9]\.[0-9]' |rev |cut -f1 -d' '|rev
}

function k8-master() {
  # print name of kubernetes cluster master
  kubectl get node --selector='node-role.kubernetes.io/master' --no-headers -o=custom-columns=NAME:.metadata.name
}

function k8-workers() {
  # print name of kubernetes cluster worker (non-master) nodes
  kubectl get node --selector='!node-role.kubernetes.io/master' --no-headers -o=custom-columns=NAME:.metadata.name
}

function fork() {
  # "for (each node in) k"(ubernetes cluster), ssh to each and run a passed in command
  # Example: `fork "uptime; uname -a"`
  for k in $(kubectl get node --no-headers -o=custom-columns=NAME:.metadata.name); do
    echo -n "---$k: "
    ssh -n $k "$@"
  done
}

function k8-ingress-ip() {
  # use the first non-master node in your cluster for 'Auto DevOps' ingress traffic
  # (and if tainted single-node cluster, it's the master)
  IP $(k8-workers |head -1)  ||  IP $(k8-master)
}


# some really nice convenience methods, some of which you'll likely find yourself using all the time 8-)

# convenience
alias kc=kubectl

function kall() {
  # shorthand status alias
  kubectl get -A pods,hpa,deployments,services,replicasets
}
function kallr() {
  # updates status every second in infin loop (CTL-Z and `kill %1` to fully kill ;-)
  # REALLY nice because you'll see huge color/background changes if/as things deploy/change...

  while true; do
    watch -dc -n 1 kubectl get -A pods,hpa,deployments,services,replicasets
  done
}
function kall-everything() {
  # like kall - but get every resource we can
  kubectl get --all-namespaces $(kc api-resources --verbs=get|cut -f1 -d' '|fgrep -v NAME|egrep -v '^events$'|sort -u|tr '\n' ,)pods
}

function kerrs() {
  # inifitely tail logs trolling for Warnings and Errors
  journalctl -ef --since='5 minutes ago' |egrep ': [EW]0'
}

function kd() {
  # kubectl describe named entity.  optional 2 args we assume is: [namespace] [name]
  if [ "$#" = "2" ]; then
    kubectl describe -n "$@"
  else
    kubectl describe "$@"
  fi
}

function kapp() {
  # Open review app in browser.
  # Pass in branch name or no arg (or '' for 1st arg) and will try to find branch name from CWD using git.
  #  'open' it into browser from command line.
  local POD=$(branch2pod "$1")
  local APP=$(kc get pod/${POD?} --no-headers -o 'custom-columns=NAME:.metadata.labels.app')

  local URL=$(kc get ingress -l app=$APP --no-headers -o='custom-columns=NAME:.spec.rules[0].host')

  echo "
  POD=$POD
  APP=$APP
  URL=$URL
"
  [ "$URL" != "" ]  &&  open http://${URL?}
}

function klogs() {
  # logically `tail -f` the logs of the review app container corresponding to the git branch
  # deduced from CWD
  local POD=$(branch2pod)
  kc logs "${POD?}" --since=1h -f |fgrep -v ' "-" "kube-probe/1.'
}

function ka() {
  # kubectl "attach" to a running container/pod -- get an ssh-like shell.
  # Pass in pod name or no arg (or '' for 1st arg) and will try to find branch name from CWD using git.
  # Looks up the pod based on the branch and auto-connects you.
  local POD="$1"
  local SHL="$2"

  [ "$POD" = "" ]  &&  POD=$(branch2pod)

  if [ "$SHL" = "" ]; then
    # no shell preference / override - try most useful shell first..
    (
      kc exec -it "$POD" zsh  ||
      kc exec -it "$POD" bash ||
      kc exec -it "$POD" sh
    )
  else
    kc exec -it "$POD" $SHL
  fi
}

function kdel() {
  # deletes the pod for the current branch
  local POD=$(branch2pod)
  kubectl delete pod/${POD?}
}

function k() {
  # handy switcher to other k8 clusters
  if [ "$1" != ""  -a  "$1" != "."  -a  -e $HOME/.kube/$1 ]; then
    [ $1  = prod ]  &&  unset  KUBECONFIG
    [ $1 != prod ]  &&  export KUBECONFIG=$HOME/.kube/$1
  fi
  env |fgrep KUBECONFIG

  echo
  echo "Config options:"
  find $HOME/.kube  -maxdepth 1 -type f |sort |rev |cut -f1 -d/ |rev |egrep -v '^config$' |perl -pe 'print "\t";'
}


function k8-slug() {
  # find GROUP-PROJECT
  local URL=$(git config --get remote.origin.url |sed 's/\.git$//')
  # may by https:// or ssh-based git url
  [[ "$URL" = https://* ]]  &&  echo $(echo "$URL" |cut -f4- -d/ |tr / -)
  [[ "$URL" = https://* ]]  ||  echo $(echo "$URL" |cut -f2  -d: |tr / -)
}


function branch2pod() {
  # Tries to print pod name for a given branch.
  # Pass in branch name or no arg (or '' for 1st arg) and will try to find branch name from CWD using git.
  # NOTE: for deployments with multiple pods, the first, alpha-sorted, is returned.
  local BRANCH="$1"
  [ "$BRANCH" = "" ]  &&  BRANCH=$(git rev-parse --abbrev-ref HEAD)

  # various characters get transformed/slugged into alternate pod/registry chars
  local BRANCH_SLUG=$(echo "$BRANCH" |tr '/_.' '-' |tr A-Z a-z)
  local SLUG=$(k8-slug)
  [ "$BRANCH_SLUG" = "master" ]  ||  SLUG=${SLUG}-${BRANCH_SLUG}


  pods |egrep -m1 "\s$SLUG$" |cut -f1
}


function deployments() {
  # outputs list of current deployment names
  resource-out  deploy
}


function deployments-urls() {
  # outputs list of current deployment website links.
  ( resource-out  ingress  'custom-columns=NAME:.spec.rules[0].host'  '--'
  ) |sort -u |perl -pe 'print "https://"'
}


function deployments-pods() {
  # outputs list of current review and production pods
  pods |cut -f1
}


function resource-out() {
  # outputs list of current deployment names
  local RESOURCE=${1:?"Usage: resource-out [resource] <output arg> <selector>"}
  local OUT=${2:-"custom-columns=NAME:.metadata.name"}
  local SELECTOR=${3:-"--selector=tier=web"}

  kc get "$RESOURCE" --no-headers -o="$OUT" "$SELECTOR" |sort -u
}


function pods() {
  # outputs TSV of: [pod name] [slug]
  resource-out pods "jsonpath={range .items[*]}{.metadata.name}{\"\t\"}{range .spec.containers[*]}{.image}{\"\n\"}{end}{end}" |perl -pe 's=/=\t=' |cut -f1,3 |cut -f1 -d: |tr / - |perl -pe 's/\-master$//'
}


function pods-with-issues() {
  # Continuously updates (@see kall) list of pods not seeming 100% healthy
  while true; do
    watch -dc -n 1 bash -c '"kubectl get -A pod" |egrep -v "(1/1|2/2|3/3)\s+Running\s+"'
  done
}


function pods-by-host() {
  # Prints pods, sorted by hostname/worker they are running on
  kc get -A pods -o wide > /tmp/$0
  for i in $(cat /tmp/$0 |col 8 |sort -u |fgrep .); do
    fgrep $i /tmp/$0
    line
  done
  rm -f /tmp/$0
}


function pods-by-host-with-issues() {
  # Prints pods not seeming 100% healthy, sorted by hostname/worker they are running on
  kc get -A pods -o wide |egrep -v '(1/1|2/2|3/3|5/5|6/6)\s+Running\s+' > /tmp/$0
  for i in $(cat /tmp/$0 |col 8 |sort -u |fgrep .); do
    fgrep $i /tmp/$0
    line
  done
  rm -f /tmp/$0
}


function delete-pod() {
  # deletes specified pod as argument
  # alt 2-arg usage is NAMESPACE POD
  [ "$#" -ge 1 ]  &&  local NS="$1"  &&  shift
  local POD=${1:?"Usage: delete-pod [pod name]  -OR-  delete-pod [namespace] [pod name]"}

  kc -n "$NS" delete pod/${POD?}
}


function k8-https-refresh() {
  # Run this to reset a deployment's ingress if https still not working after a few minutes.
  # This can happen esp. if _not_ using DNS wildcards and hit timing issue, and lets encrypt
  # tries to setup before the deployment hostname DNS resolves...
  local ING=${1:?"Usage: k8-https-refresh [deploy name, eg: ia-petabox-spring-cleaning]"}
  kc get $ING -o yaml |tee ing.yml
  kc delete $ING
  kc create -f ing.yml
  rm ing.yml
}
